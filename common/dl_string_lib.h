#ifndef DL_STRING_LIB_H__
#define DL_STRING_LIB_H__

#include "dl_extern_lib.h"

#include <string.h>


#define MEMCHR		0
#define MEMCMP		1
#define MEMCPY		2
#define MEMMOVE		3
#define MEMSET		4
#define STRCAT		5
#define STRNCAT		6
#define STRCHR		7
#define STRCMP		8
#define STRNCMP		9
#define STRCOLL		10
#define STRCPY		11
#define STRNCPY		12
#define STRCSPN		13
#define STRERROR	14
#define STRLEN		15
#define STRPBRK		16
#define STRRCHR		17
#define STRSPN		18
#define STRSTR		19
#define STRTOK		20
#define STRXFRM		21




#endif
